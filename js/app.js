$(document).foundation();


// JavaScript Document
(function() {
"use strict";

var top = document.querySelector("#btt");
function backTop(){
scroll=0;
window.scrollTo(0, 0);

}


top.addEventListener("click", backTop, false);

//Timeline
var events = document.querySelectorAll(".event");
var eventInfo = document.querySelector(".eventInfo");
var date = document.querySelector(".bioHeading");
var desc = eventInfo.querySelector("p");
var	appliedClass;

console.log(events);

function eventChange(evt) {
    console.log(this.id);
    
    evt.preventDefault();
    let objectIndex = dynamicContent[this.id];

    desc.firstChild.nodeValue = objectIndex.text;
    date.firstChild.nodeValue = objectIndex.date;

    appliedClass = this.id;
}
    
events.forEach(function(element, index) {
   element.addEventListener('click', eventChange, false);
});

//Gallery
var images = document.querySelectorAll(".galleryImages");

function popLightbox() {
    //debugger;
    window.scrollTo(0, 0);
    document.body.style.overflow = "hidden";

    let lightbox = document.querySelector('.lightbox');
    lightbox.style.display = 'block';

    let lightboxImg = lightbox.querySelector('img');
    let lightboxClose = lightbox.querySelector('.close-lightbox');

    lightboxImg.src = "images/obj-" + this.id + ".jpg";

    lightboxClose.addEventListener('click', closeLightbox, false);
}

function closeLightbox() {
    //debugger;
    document.body.style.overflow = "scroll";
    let lightbox = document.querySelector('.lightbox');
    lightbox.style.display = "none";
    let lightboxImg = lightbox.querySelector('img');
    lightboxImg.src = "";
}

for (var i=0; i<images.length; i++) {
    images[i].addEventListener("click", popLightbox, false);
}

//video controls
var vid = document.querySelector("#videoBox");
var controls = document.querySelector("#videoControls");
var toggle = controls.querySelector("#play");
var slider = controls.querySelector("#slider");
var ctime = controls.querySelector("#timeCurrent");
var dtime = controls.querySelector("#timeDuration");
var volume = controls.querySelector("#volume");
var full = controls.querySelector("#fullscreen");

function toggleVid(evt)
{
    if(video.paused) {
        video.play();
        evt.currentTarget.innerHTML = "Pause";
    }
    else{
        video.pause();
        evt.currentTarget.innerHTML = "Play";
    }
}

function slideTime()
{
    var seek = vid.duration + (slider.value / 100);
    vid.currentTime = seek;
}

function seekTime()
{
    var time = vid.currentTime * (100/vid.duration);
    slider.value = time;
    var currentMins = Math.floor(vid.currentTime / 60);
    var currentSecs = Math.floor(vid.currentTime - currentMins * 60);
    var durationMins = Math.floor(vid.currentTime / 60);
    var durationSecs = Math.floor(vid.currentTime - durationMins * 60);
    if(currentSecs < 10) {
        currentSecs = "0" + currentSecs;
    }
    if(durationSecs < 10) {
        durationSecs = "0" + durationSecs;
    }
    ctime.innerHTML = currentMins + ":" + currentSecs;
    dtime.innerHTML = durationMins + ":" + durationSecs;
}

function slideVolume()
{
    vid.volume = volume.value / 100;
}

function fullscreen()
{
    if(vid.requestFullScreen){
        vid.requestFullScreen();
    } else if(vid.webkitRequestFullScreen){
        vid.webkitRequestFullScreen();
    } else if(vid.mozRequestFullScreen){
        vid.webkitRequestFullScreen();
    }
}

toggle.addEventListener("click", toggleVid, false);
slider.addEventListener("change", slideTime, false);
vid.addEventListener("timeupdate", seektime, false);
volume.addEventListener("change", slideVolume, false);
full.addEventListener("click", fullscreen, false);
})();
