var dynamicContent = {
		
		event1 : {
			text : "Odell Beckham Jr. is a wide-reciever for the New York Giants. Ever since his career began,  Odell has been breaking records, astounding crowds with his amazing plays, and elevating the Giants to whole new levels, even guaranteeing them a seat in the playoffs for the first time since 2011. Odell has also been known to be a trendsetter in fashion and a charitable soul; donating to many charities including the Make a Wish Foundation.",
			
			headline : "Bio",
			date: "Bio"
		},
	
		event2 : {
			text : "Odell Beckham Jr was born November 5th, 1992 in Baton Rouge Louisianna. Odell is the son of his father Odell Beckham Sr. and his mother Heather Van Norman.",
			
			headline : "Commitment",
			date: "Novermber 05, 1992"
		},

		event3 : {
			text : "Odell Beckham Jr. commits to LSU Tigers.",
			
			headline : "Commitment",
			date: "January 08, 2011"
		},

		event4 : {
			text : "Odell Beckham Jr. named to 2013 CBSSports.com All-America Team (first team).",
			
			headline : "Accolades",
			date: "December 17, 2013"
		},

		event5 : {
			text : "The New York Giants select Odell Beckham Jr. with the No. 12 overall pick in the first round of the 2014 NFL Draft.",
			
			headline : "Draft",
			date: "May 08, 2014"
		},
		
		event6: {
			text : "During a game against the Dallas Cowboys on Sunday Night Football, Beckham had 10 catches for 146 yards and two touchdowns, including a one-handed touchdown reception hailed as the 'catch of the year'.",

			headline : "Catch of the year",
			date: "November 23, 2014"
		}
	};
